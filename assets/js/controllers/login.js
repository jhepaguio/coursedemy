/*document.getElementById('login').addEventListener("submit", (e) => {
	e.preventDefault()

	let emailField = document.getElementById('login-email').value;
	let passwordField = document.getElementById('login-password').value;

	if(emailField === "" || passwordField === ""){
		alert("Fields cannot be empty")
	}else{
		fetch('http://localhost:4000/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: emailField,
				password: passwordField
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data == true){
				window.location.replace("./pages/profile.html")
			} else {
				alert('User does not exist')
			}
		})
	}
})*/


let loggedInCredentials = localStorage.getItem("id")

if(!loggedInCredentials || loggedInCredentials == null){
	let loginForm = document.getElementById('login');

	//if loginForm is submitted
	loginForm.addEventListener('submit', (e) => {
		e.preventDefault()

		//assign tge values firstname and lastname to variables below
		let email = document.getElementById('login-email').value;
		let password = document.getElementById('login-password').value;

		//if firstname is an empty string or lastname is an empty string
		if(email == "" || password == "") {
			//prompt 
			alert("Cannot leave fields empty")
		}
		else {
			fetch('https://sheltered-journey-82575.herokuapp.com/api/users/login', {
				// fetch('http://localhost:3000/api/users/login', {
				method: 'POST',
				headers: { 
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email: email,
					password: password
				})
			})
			.then(res => {
				return res.json()
			})
			.then(data => {
				if (data.userLogin && data.userLogin.isActive == false){
					localStorage.clear()
					alert('User has been deactivated')
				} 
				 else if (data.userLogin && data.userLogin.isActive == true) {
          	//set the global user state to have properties containing authenticated user's ID and role
          	// console.log(data.userLogin)

          	localStorage.setItem("id", data.userLogin._id)
          	localStorage.setItem("firstName", data.userLogin.firstName)
          	localStorage.setItem("lastName", data.userLogin.lastName)
          	localStorage.setItem("email", data.userLogin.email)
          	localStorage.setItem("mobile", data.userLogin.mobileNo)
          	localStorage.setItem("course", data.userLogin.enrollents.courseID)
          	localStorage.setItem("isActive", data.userLogin.isActive)

        	
          	window.location.replace("./pages/profile.html")
          } else {
          	alert("User does not exist")
          } 

      })

		}
	})
} else {
	window.location.replace('./pages/profile.html')
	// console.log(localStorage.getItem("id"))
	// console.log(localStorage.getItem("email"))
	console.log(localStorage.getItem("course"))
	 console.log(localStorage.getItem("isActive"))

}


/*
let loggedInCredentials = localStorage.getItem("id")
let isActive = localStorage.getItem("isActive")

if((!loggedInCredentials || loggedInCredentials == null) && (isActive == true)){
	let loginForm = document.getElementById('login');

	//if loginForm is submitted
	loginForm.addEventListener('submit', (e) => {
		e.preventDefault()

		//assign tge values firstname and lastname to variables below
		let email = document.getElementById('login-email').value;
		let password = document.getElementById('login-password').value;

		//if firstname is an empty string or lastname is an empty string
		if(email == "" || password == "") {
			//prompt 
			alert("Cannot leave fields empty")
		}
		else {
			// fetch('https://sheltered-journey-82575.herokuapp.com/api/users/login', {
				fetch('http://localhost:3000/api/users/login', {
					method: 'POST',
					headers: { 
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						email: email,
						password: password
					})
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					if (data.userLogin) {
          	//set the global user state to have properties containing authenticated user's ID and role
          	// console.log(data.userLogin)

          	localStorage.setItem("id", data.userLogin._id)
          	localStorage.setItem("firstName", data.userLogin.firstName)
          	localStorage.setItem("lastName", data.userLogin.lastName)
          	localStorage.setItem("email", data.userLogin.email)
          	localStorage.setItem("course", data.userLogin.enrollents)
          	localStorage.setItem("isActive", data.userLogin.isActive)

          	if ((localStorage.getItem("isActive") == false){
          		alert("User has been deactivated")
          	} else {
          		window.location.replace("./pages/profile.html")
          	})
          } else {
          	alert("User does not exist")
          } 

      })

			}
		})
} else {
	window.location.replace('./pages/profile.html')
	// console.log(localStorage.getItem("id"))
	// console.log(localStorage.getItem("email"))
	// console.log(localStorage.getItem("course"))
	// console.log(localStorage.getItem("isActive"))

}
*/