let editForm = document.getElementById('edit-form');
let id = localStorage.getItem("id");
  //if editForm is submitted
  document.getElementById('edit-first-name').value = localStorage.getItem('firstName');
  document.getElementById('edit-last-name').value = localStorage.getItem('lastName');
  document.getElementById('edit-email').value = localStorage.getItem('email');
  document.getElementById('edit-mobile').value = localStorage.getItem('mobile');

editForm.addEventListener('submit', (e) => {
  console.log("test");
  e.preventDefault();

  //assign tge values firstname and lastname to variables below
  let firstName = document.getElementById('edit-first-name').value;
  let lastName = document.getElementById('edit-last-name').value;
  let email = document.getElementById('edit-email').value;
  let password = document.getElementById('edit-password').value;
  let confirmPassword = document.getElementById('confirm-password').value;
  let mobile = document.getElementById('edit-mobile').value;


  if(password != confirmPassword) {
    alert("Passwords does not match")
  } else if(firstName == "" || lastName == "" || email == "" || mobile == "" ) {
    alert("All fields are required")
  } else {
    fetch(`https://sheltered-journey-82575.herokuapp.com/api/users/${id}/update`, {
    // fetch(`http://localhost:3000/api/users/${id}/update`, {
      method: 'PUT',
      headers: { 
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password,
        mobileNo: mobile
      })
    })
    .then(res => {
      return res.json()
    })
    .then(data => {
      if (data.user) {
          localStorage.setItem("firstName", data.user.firstName)
          localStorage.setItem("lastName", data.user.lastName)
          localStorage.setItem("email", data.user.email)
          localStorage.setItem("mobile", data.user.mobileNo)
        
          window.location.replace("./profile.html")
          alert("Successfully updated!")
        } else {
          alert("Oops! Something went wrong!")
        } 

    })
  }
})