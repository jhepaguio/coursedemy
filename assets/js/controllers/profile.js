let loggedInCredentials = localStorage.getItem("id")

//if the loggedInCredentials is false - meaning walang laman OR
// if(!loggedInCredentials || loggedInCredentials == null){
// 	alert("Login first!")
// 	window.location.replace('../index.html')
// } else {
	let firstName = localStorage.getItem("firstName")[0].toUpperCase() + localStorage.getItem("firstName").substr(1)
	let lastName = localStorage.getItem("lastName")[0].toUpperCase() +localStorage.getItem("lastName").substr(1)
	let email = localStorage.getItem("email")

	document.getElementById('profile-name').innerHTML = `<strong>${lastName}, ${firstName}</strong>`
	document.getElementById('email-section').innerHTML = `${email}`

let courseList = document.getElementById('courseList')

fetch('https://sheltered-journey-82575.herokuapp.com/api/users/') 
// fetch('http://localhost:3000/api/users/') 
.then(response => response.json())
.then((data)=> {
	let courseData; 
			courseData = data.map(course => {
				let courseItem = localStorage.getItem("course")

			if (course.courseID) {
				// console.log(course.enrollents.courseID)
				let courseItem = `<li class='list-group-item'>${courseItem}| enrolled on: ${course.enrollents.enrolledOn}</li>`
				return courseItem;
			}
		}).join("")
	
	courseList.innerHTML = courseData;
})


	//logout button
	let logoutForm = document.getElementById('logoutForm')
	logoutForm.innerHTML = `<button type="submit" class="btn btn-link ml-auto logout-btn"> Log out</button>`

	logoutForm.addEventListener('submit', (e)=> {
		e.preventDefault()
		localStorage.clear()
		console.log(localStorage.getItem("id"))
		if (localStorage.getItem("id") == null){
		window.location.replace('../index.html')}

	})
// }


