let registration = document.querySelector("#registration")

registration.addEventListener('submit', (e) => {
	e.preventDefault()

	let firstName = document.querySelector("#inputName1").value
	let lastName = document.querySelector("#inputName2").value
	let email = document.querySelector("#inputEmail").value
	let mobileNo = document.querySelector("#inputTel").value
	let password1 = document.querySelector("#inputPassword1").value
	let password2 = document.querySelector("#inputPassword2").value
	let course = document.querySelector('#inputCourse').value
	let date = document.querySelector('#inputDate').value

	if( firstName !== "" && lastName !== "" && password1 !== "" && email !== "" && mobileNo !== ""){
		if (password1 == password2) {
			fetch('https://sheltered-journey-82575.herokuapp.com/api/users/emailCheck', {
			// fetch('http://localhost:3000/api/users/emailCheck', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email: email
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)
				if(data === false){
					fetch('https://sheltered-journey-82575.herokuapp.com/api/users/', {
						// fetch('http://localhost:3000/api/users/', {
							method: 'POST',
							headers: {
								'Content-Type': 'application/json'
							},
							body: JSON.stringify({
								firstName: firstName,
								lastName: lastName,
								email: email,
								password: password1,
								mobileNo: mobileNo,
								courseId: course,
								enrolledOn: date
							})
						})
					.then(res => {
						return res.json()
					})
					.then(data => {
						if(data === true){
							alert("Registered successfully")
							window.location.replace("../index.html")
						}else{
							alert("Somethig went wrong")
						}
					})
				} else if (data === true){
					alert('Email is already in use')
				}
			})
		} else {
			alert("Passwords do not match")
		}
	}else{
		alert("Fill in missing fields")
	}
})