let deactivate = document.querySelector("#deactivate");
let id = localStorage.getItem("id");
let isActive = localStorage.getItem("isActive");


deactivate.addEventListener('submit', (e) => {
	e.preventDefault();

	fetch(`https://sheltered-journey-82575.herokuapp.com/api/users/${id}`, { 
		// fetch('http://localhost:3000/api/users/${id}', {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			}
		})
		.then(response => response.json())
		.then((data)=> {
			console.log(data)
			if(data._id==null){
				alert('User account has been deactivated')
				localStorage.clear()
				window.location.replace('../index.html')
			} else {
				alert('Something went wrong')
			}
		})
	})